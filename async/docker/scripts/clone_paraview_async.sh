#! /bin/sh
set -e

git clone https://gitlab.kitware.com/async/paraview.git "paraview-async/src" 

cd paraview-async/src

git checkout v0.9.0

git submodule update --init --recursive
