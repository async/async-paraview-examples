#!/usr/bin/env bash
SCRIPT_DIR=`dirname "$0"`

set -e

cd $SCRIPT_DIR
./development/build.sh
./runtime/build.sh
