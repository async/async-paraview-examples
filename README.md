# Reference example for Asynchronous ParaView

## Introduction

This repository provides our showcase on how to use and leverage [Asynchronous ParaView backend](https://gitlab.kitware.com/async/paraview) for performing data processing and rendering.

While the initial reference examples mainly focus on highlighting the new capabilities of our asynchronous library, they also provide enough controls and monitoring to experiment with various streaming parameters. With time, the repository will expand with additional reference examples covering advanced concepts and/or features that will be added to our asynchronous backend service.

## Content

This repository contains trame-based reference example driven by the [Asynchronous ParaView as a Service backend](https://gitlab.kitware.com/async/paraview).
In order to compare the responsiveness and performance, we also provide apps that use the traditional ParaView (synchronous) backend.

Current set of reference examples:
1. [async/python/wavelet.py](async/python/wavelet.py)
    A visualization of a wavelet source with the contour, clip, slice and threshold filters. This application aims to give you control on arbitrary data size for exploring interuptability and separation of services. Some progress feedback let you monitor the state of the various services, server and client availability. Then additional controls and monitoring are available to adjust image delivery parameters so you can investigate bandwidth and FPS.
2. [async/python/rock.py](async/python/rock.py)
    A visualization pipeline composed on filters applied on a rock sample which represent a more realistic data exploration while using most of the same control and monitoring as the wavelet example.

See [async/python/wavelet.py](async/python/wavelet.py) for detailed commented source code. The [async/python/rock.py](async/python/rock.py) has similar code structure.

## Async backend

The async backend leverages the [Asynchronous ParaView as a Service](https://gitlab.kitware.com/async/paraview) that offers concurrent data processing and rendering pipelines, asynchronous services and interruptable filters that improve responsiveness and the overall user experience.
See [Run instructions](async/README.md) for details. If you aim to build your own docker image you can follow [those instructions](async/docker/README.md).

## Sync backend

For comparison, the same example applications have been implemented using the traditional (synchronous) ParaView backend. The applications can be ran using a ParaView binary or by building a docker image. See [Run instructions](sync/README.md) for details.

## License

The reference examples are provided under the OSI-approved Apache 2 License.
See [LICENSE][] for details.

[LICENSE]: LICENSE