#!/bin/sh
set -e
set -x

readonly type=$1
readonly version="5.11.0-RC2"
readonly shatool="sha256sum"
case $type in
    egl)
        sha256sum="5a090d1f198a965290004c449a9b1ec2818b877b640503c21e33f9a869294394"
        ;;
    osmesa)
        sha256sum="3e987f698ee89e96dabab10161f256032593acf3b7b52ab8bdaccdf05b820c5c"
        ;;
    *)
        echo "Unrecognized type $type"
        exit 1
        ;;
esac 

readonly tarball="ParaView-${version}-${type}-MPI-Linux-Python3.9-x86_64.tar.gz" 

readonly url="https://www.paraview.org/paraview-downloads/download.php?submit=Download&version=v5.11&type=binary&os=Linux&downloadFile=${tarball}"

curl -L "$url" --output ${tarball}
echo "$sha256sum  $tarball" > paraview.sha256sum
$shatool --check paraview.sha256sum

mkdir -p /opt/paraview/runtime
tar xf "$tarball" --strip-components=1 -C /opt/paraview/runtime

rm $tarball
